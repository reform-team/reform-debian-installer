#!/bin/sh
#
# Copyright 2023 Johannes Schauer Marin Rodrigues <josch@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

set -eu

export LC_ALL=C.UTF-8

: "${DIST:=bookworm}"
: "${MIRROR:=reform.debian.net}"

usage() {
  echo "usage: $0 [-m mirror] [-d dist] board" >&2
  exit 1
}

usage_error() {
  echo "error: $*" 1>&2
  usage
}

while getopts :h:d:m:-: OPTCHAR; do
  case "$OPTCHAR" in
    d) DIST="$OPTARG" ;;
    m) MIRROR="$OPTARG" ;;
    h) usage ;;
    -) case "$OPTARG" in
      help) usage ;;
      dist)
        DIST="$(nth_arg "$OPTIND" "$@")"
        OPTIND=$((OPTIND + 1))
        ;;
      dist=*) DIST="${OPTARG#*=}" ;;
      mirror)
        MIRROR="$(nth_arg "$OPTIND" "$@")"
        OPTIND=$((OPTIND + 1))
        ;;
      mirror=*) MIRROR="${OPTARG#*=}" ;;
      *)
        echo "unrecognized option --$OPTARG" >&2
        exit 1
        ;;
    esac ;;
    :) usage_error "missing argument for -$OPTARG" ;;
    '?') usage_error "unrecognized option -$OPTARG" ;;
    *)
      echo "internal error while parsing command options" >&2
      exit 1
      ;;
  esac
done
shift "$((OPTIND - 1))"

if [ "$#" -ne 1 ]; then
  usage_error "exactly one positional argument required"
else
  SYSIMAGE="$1"
fi

case "$SYSIMAGE" in
  pocket-reform-system-a311d) : ;;
  pocket-reform-system-imx8mp) : ;;
  reform-system-a311d) : ;;
  reform-system-imx8mp) : ;;
  reform-system-imx8mq) : ;;
  reform-system-ls1028a) : ;;
  reform-system-rk3588) : ;;
  reform-next-system-rk3588) : ;;
  pocket-reform-system-rk3588) : ;;
  *)
    echo "unsupported image: $SYSIMAGE" >&2
    echo >&2
    echo "List of supported images:" >&2
    echo >&2
    echo "pocket-reform-system-a311d" >&2
    echo "pocket-reform-system-imx8mp" >&2
    echo "reform-system-a311d" >&2
    echo "reform-system-imx8mp" >&2
    echo "reform-system-imx8mq" >&2
    echo "reform-system-ls1028a" >&2
    echo "reform-system-rk3588" >&2
    echo "reform-next-system-rk3588" >&2
    echo "pocket-reform-system-rk3588" >&2
    exit 1
    ;;
esac

cleanup() {
  rm -f Release Release.gpg SHA256SUMS cpio.modules.gz cpio.preseed.gz \
    extlinux.conf initrd-full.gz initrd initrd.gz preseed.cfg \
    "reform_${DIST%-backports}.sources" "reform_${DIST%-backports}-backports.sources" \
    flash.bin ls1028a-mhdpfw.bin cpiofilter.py machines/*.conf
  rm -rf usr var
}

rm -f partition.img partition.img.bmap
cleanup

# shellcheck disable=SC2064
trap "rm -f reform_${DIST%-backports}.sources reform_${DIST%-backports}-backports.sources" EXIT INT TERM
for bpo in "" "-backports"; do
  case $MIRROR in
    mntre.com)
      case "$DIST" in *-backports) echo "backports not supported for mntre.com mirror" >&2; exit 1;; esac
      [ "$bpo" = "-backports" ] && continue
      cat <<END >"reform_${DIST}.sources"
Types: deb
URIs: https://mntre.com/reform-debian-repo
Suites: reform
Components: main
Architectures: arm64
Trusted: yes
END
      ;;
    reform.debian.net)
      cat <<END >"reform_${DIST%-backports}${bpo}.sources"
Types: deb
URIs: https://reform.debian.net/debian
Suites: ${DIST%-backports}${bpo}
Components: main
Architectures: arm64
Signed-By:
END
      sed -n '/^-----BEGIN PGP PUBLIC KEY BLOCK-----$/,$s/^/ /p' "$0" >>"reform_${DIST%-backports}${bpo}.sources"
      ;;
  esac
done

CONF="$(grep -l "^SYSIMAGE=\"$SYSIMAGE\"\$" /usr/share/reform-tools/machines/*.conf || :)"
if [ ! -e "$CONF" ]; then
  DTBPATH="i-do-no-exist"
else
  # shellcheck source=/dev/null
  . "$CONF"
fi

# run this script without mmdebstrap isolation if there is exactly a single
# match for each of the globs
all_found=1
counter=0
case $DIST in
  bookworm)
    abiname="-reform2-arm64"
    ;;
  bookworm-backports)
    abiname="+bpo-mnt-reform-arm64"
    ;;
  trixie)
    abiname="-mnt-reform-arm64"
    ;;
  *)
    echo "unsupported distribution: $DIST" >&2
    exit 1
    ;;
esac
for p in /lib/modules/*"$abiname/" \
  /usr/lib/linux-image-*"$abiname/$DTBPATH" \
  /boot/vmlinuz-*"$abiname"; do
  if [ ! -e "$p" ]; then
    echo "E: No match for: $p" >&2
    all_found=0
    break
  fi
  counter=$((counter + 1))
done

if [ "$all_found" -eq 0 ] || [ "$counter" -ne 3 ]; then
  if [ "$all_found" -eq 0 ]; then
    echo "I: not all globs matched -- running inside mmdebstrap wrapper" >&2
  elif [ "$counter" -gt 3 ]; then
    echo "I: globs did match too many files -- running inside mmdebstrap wrapper" >&2
  elif [ "$counter" -lt 3 ]; then
    echo "I: globs did match too few files -- running inside mmdebstrap wrapper" >&2
  fi
  if [ -n "${MMDEBSTRAP_HOOK+x}" ]; then
    echo "E: refusing to run nested mmdebstrap" >&2
    exit 1
  fi
  set --
  case "$DIST" in
    bookworm) set -- "$@" --include=linux-image-arm64 ;;
    bookworm-backports)
      set -- "$@" --include=linux-image-mnt-reform-arm64 \
        --setup-hook="copy-in reform_${DIST%-backports}-backports.sources /etc/apt/sources.list.d"
      ;;
    trixie) set -- "$@" --include=linux-image-mnt-reform-arm64 ;;
  esac
  case $MIRROR in
    reform.debian.net)
      # shellcheck disable=SC2016
      set -- "$@" --setup-hook='{ echo "Package: *"; echo "Pin: origin \"reform.debian.net\""; echo "Pin-Priority: 999"; } > "$1"/etc/apt/preferences.d/reform.pref'
      ;;
    mntre.com)
      # shellcheck disable=SC2016
      set -- "$@" --setup-hook='{ echo "Package: *"; echo "Pin: release n=reform, l=reform"; echo "Pin-Priority: 999"; } > "$1"/etc/apt/preferences.d/reform.pref'
      ;;
    *)
      echo "unsupported mirror: $MIRROR" >&2
      exit 1
      ;;
  esac
  # shellcheck disable=SC2016
  mmdebstrap --variant=apt --architecture=arm64 \
    --include=mtools,dosfstools,fdisk,ca-certificates,python3,reform-tools,bmap-tools \
    --setup-hook='mkdir -p "$1"/etc/apt/sources.list.d' \
    --setup-hook="copy-in reform_${DIST%-backports}.sources /etc/apt/sources.list.d" \
    --customize-hook='chroot "$1" useradd --home-dir /home/user --create-home user' \
    --customize-hook="copy-in '$0' /home/user" \
    --chrooted-customize-hook="env --chdir=/home/user runuser -u user -- ./$(basename "$0") -d $DIST -m $MIRROR $SYSIMAGE" \
    --customize-hook='copy-out /home/user/partition.img .' \
    --customize-hook='copy-out /home/user/partition.img.bmap .' \
    "$@" "${DIST%-backports}" /dev/null
  exit 0
fi

case "$DIST" in
  bookworm)
    KVER="$(dpkg-query --showformat='${Version}' --show linux-image-arm64)"
    case "$KVER" in
      *~bpo12+1+reform*)
        echo "wrong kernel version: $KVER" >&2
        exit 1
        ;;
      *+reform*) : ;;
      *)
        echo "wrong kernel version: $KVER" >&2
        exit 1
        ;;
    esac
    ;;
  bookworm-backports)
    KVER="$(dpkg-query --showformat='${Version}' --show linux-image-mnt-reform-arm64)"
    case "$KVER" in
      *~bpo12+1+reform*) : ;;
      *+reform*)
        echo "wrong kernel version: $KVER" >&2
        exit 1
        ;;
      *)
        echo "wrong kernel version: $KVER" >&2
        exit 1
        ;;
    esac
    ;;
  trixie)
    KVER="$(dpkg-query --showformat='${Version}' --show linux-image-mnt-reform-arm64)"
    case "$KVER" in
      *+reform*) : ;;
      *)
        echo "wrong kernel version: $KVER" >&2
        exit 1
        ;;
    esac
    ;;
  *)
    echo "unsupported distribution: $DIST" >&2
    exit 1
    ;;
esac

trap cleanup EXIT INT TERM

set -x

case "$DIST" in
  bookworm*)
    /usr/lib/apt/apt-helper -oAPT::Sandbox::User=root download-file "http://deb.debian.org/debian/dists/${DIST%-backports}/Release" Release
    /usr/lib/apt/apt-helper -oAPT::Sandbox::User=root download-file "http://deb.debian.org/debian/dists/${DIST%-backports}/Release.gpg" Release.gpg
    gpgv --quiet --keyring=/usr/share/keyrings/debian-archive-keyring.gpg Release.gpg Release
    /usr/lib/apt/apt-helper -oAPT::Sandbox::User=root download-file "http://deb.debian.org/debian/dists/${DIST%-backports}/main/installer-arm64/current/images/SHA256SUMS" SHA256SUMS
    sed -ne 's/^ \([a-f0-9]\{64\}\)\s\+[1-9][0-9]* main\/installer-arm64\/current\/images\/SHA256SUMS/\1  SHA256SUMS/p' Release | sha256sum --check
    /usr/lib/apt/apt-helper -oAPT::Sandbox::User=root download-file "http://deb.debian.org/debian/dists/${DIST%-backports}/main/installer-arm64/current/images/netboot/debian-installer/arm64/initrd.gz" initrd.gz
    sed -ne 's/^\([a-f0-9]\{64\}\)  \.\/netboot\/debian-installer\/arm64\/initrd\.gz/\1  initrd.gz/p' SHA256SUMS | sha256sum --check
    ;;
  *)
    echo "Performing initrd.gz download without gpg verification" >&2
    /usr/lib/apt/apt-helper -oAPT::Sandbox::User=root download-file https://d-i.debian.org/daily-images/arm64/daily/netboot/debian-installer/arm64/initrd.gz initrd.gz
    ;;
esac

/usr/lib/apt/apt-helper -oAPT::Sandbox::User=root download-file \
  "https://source.mnt.re/reform/$UBOOT_PROJECT/-/jobs/artifacts/$UBOOT_TAG/raw/$(basename "$DTBPATH" .dtb)-flash.bin?job=build" \
  "flash.bin" "SHA1:$UBOOT_SHA1"

# download DisplayPort firmware blob for LS1028A
if [ "$SYSIMAGE" = "reform-system-ls1028a" ]; then
  /usr/lib/apt/apt-helper download-file \
    "https://source.mnt.re/reform/reform-ls1028a-uboot/-/raw/main/ls1028a-mhdpfw.bin" \
    ls1028a-mhdpfw.bin "SHA1:fa96b9aa59d7c1e9e6ee1c0375d0bcc8f8e5b78c"
fi

{
  # our extra packages
  # our kernel headers are named differently for bookworm-backports and later
  case "$DIST" in
    bookworm)                  echo d-i pkgsel/include string reform-tools flash-kernel linux-headers-arm64 ;;
    bookworm-backports|trixie) echo d-i pkgsel/include string reform-tools flash-kernel linux-headers-mnt-reform-arm64 ;;
    *) echo "unsupported distribution: $DIST" >&2; exit 1 ;;
  esac
  # auto-answer message about d-i knowing about different kernel modules than
  # the ones we patched into the initrd
  echo d-i anna/no_kernel_modules boolean true
  # our kernel is named differently for bookworm-backports and later
  case "$DIST" in
    bookworm) : ;;
    bookworm-backports|trixie) echo d-i base-installer/kernel/image string linux-image-mnt-reform-arm64 ;;
    *) echo "unsupported distribution: $DIST" >&2; exit 1 ;;
  esac
} > preseed.cfg

cat <<'END' >extlinux.conf
default l0
menu title Debian-Installer
prompt 0
timeout 50

label l0
menu label Debian-Installer
linux /vmlinuz
initrd /initrd.gz
fdtdir /dtbs/
append ro no_console_suspend cryptomgr.notests ${bootargs} console=tty1
END

# instead of writing to var/lib/dpkg/info/${pkg}.templates we could also write
# out this file on-the-fly to an arbitrary location and then load it right
# after by running debconf-loadtemplate $pkg /path/to/tmp.templates
mkdir -p var/lib/dpkg/info
cat <<'END' >var/lib/dpkg/info/reform.templates
Template: reform/choose_machine
Type: select
Choices: Normal (single), HDMI (dual)
Default: Normal (single)
Description: Choose the initial device tree
 This choice influences whether your Reform has HDMI enabled or not in addition
 to the internal display. The disadvantage of having HDMI enabled is that this
 results in the internal display being run by a less performant display engine
 (LCDIF instead of DCSS) and as a result, the panel is run at a lower
 frequency. Choose "Normal (single)" if you do not need HDMI support. Use the
 reform-display-config tool to change this setting later on.

Template: reform/choose_uboot_device
Type: select
Choices: no u-boot, ${MY_CHOICES}, none of the above
Default: no u-boot
Description: Choose on which device to flash U-Boot
 You probably already have U-Boot installed on your device or otherwise you
 would not be able to boot into the Debian installer. If you are happy with how
 things are, keep the default "no u-boot" or choose "none of the above".
 Otherwise, choose a device to write the latest stable U-Boot to. This can also
 be used to update U-Boot to the latest stable version. After installation, use
 the reform-flash-uboot tool to update U-Boot.
END

mkdir -p usr/lib/post-base-installer.d
{
  cat <<END
#!/bin/sh
set -e
mkdir -p /target/etc/apt/sources.list.d
cp -t /target/etc/apt/sources.list.d /reform_${DIST%-backports}.sources
END
  case $DIST in *-backports) echo "cp -t /target/etc/apt/sources.list.d /reform_${DIST%-backports}-backports.sources" ;; esac
  echo "mkdir -p /target/etc/apt/preferences.d"
  case $MIRROR in
    reform.debian.net) echo '{ echo "Package: *"; echo "Pin: origin \"reform.debian.net\""; echo "Pin-Priority: 999"; } > /target/etc/apt/preferences.d/reform.pref' ;;
    mntre.com)         echo '{ echo "Package: *"; echo "Pin: release n=reform, l=reform"; echo "Pin-Priority: 990"; } > /target/etc/apt/preferences.d/reform.pref' ;;
    *) echo "unsupported mirror: $MIRROR" >&2; exit 1;;
  esac
  echo 'apt-install ca-certificates'
  echo 'in-target apt-get update --error-on=any'
} > usr/lib/post-base-installer.d/99reform
chmod +x usr/lib/post-base-installer.d/99reform

# 07preseed runs preseed/late_command
# 20final-message prints one last message
# 95umount unmounts
# 99reboot reboots
# we put our finish-install script at the very beginning
mkdir -p usr/lib/finish-install.d
cat <<'END' >usr/lib/finish-install.d/00reform
#!/bin/sh
set -eu

. /usr/share/debconf/confmodule

case "$(cat /proc/device-tree/model)" in
	"MNT Reform 2"|"MNT Reform 2 HDMI")
		db_input high reform/choose_machine || true
		db_go || exit 10
		db_get reform/choose_machine
		if [ "$RET" = "HDMI (dual)" ]; then
			echo "MNT Reform 2 HDMI" > /target/etc/flash-kernel/machine
		else
			echo "MNT Reform 2" > /target/etc/flash-kernel/machine
		fi

		# We need to write out /etc/default/flash-kernel after installing flash-kernel
		# or otherwise an ucf prompt will be displayed to the user. After updating the
		# file, flash-kernel needs to be run again.
		{ echo 'LINUX_KERNEL_CMDLINE="loglevel=7"'; echo 'LINUX_KERNEL_CMDLINE_DEFAULTS=""'; } \
			> /target/etc/default/flash-kernel
		in-target flash-kernel
		;;
esac

# copy uboot flash.bin binary to /boot
cp -t /target/boot /flash.bin
case "$(cat /proc/device-tree/model)" in "MNT Reform 2 with LS1028A Module")
	cp -t /target/boot /ls1028a-mhdpfw.bin
esac

# FIXME: fill MY_CHOICES with entries that look more like the grub menu
[ -e "/target/usr/share/reform-tools/machines/$(cat /proc/device-tree/model).conf" ] || exit 10
. "/target/usr/share/reform-tools/machines/$(cat /proc/device-tree/model).conf"

# EMMC_BOOT can be "warn" which emits a warning in reform-flash-uboot
if [ "$EMMC_BOOT" != false ] && [ "$SD_BOOT" = true ]; then
	db_subst reform/choose_uboot_device MY_CHOICES "eMMC, SD-card"
elif [ "$SD_BOOT" = true ]; then
	db_subst reform/choose_uboot_device MY_CHOICES "SD-card"
elif [ "$EMMC_BOOT" != false ]; then
	db_subst reform/choose_uboot_device MY_CHOICES "eMMC"
fi
db_input high reform/choose_uboot_device || true
db_go || exit 10
db_get reform/choose_uboot_device
case "$RET" in
	eMMC)    in-target reform-flash-uboot --offline emmc;;
	SD-card) in-target reform-flash-uboot --offline sd;;
esac
END
chmod +x usr/lib/finish-install.d/00reform

cat <<'END' >cpiofilter.py
#!/usr/bin/env python3

import sys
import struct


def main():
    written_out = 0
    for i, initrdfname in enumerate(sys.argv[1:]):
        with open(initrdfname, "rb") as f:
            while True:
                buffer = b""
                if f.tell() % 4 != 0:
                    f.read(4 - f.tell() % 4)
                # we might've removed files, so we have to recompute the padding
                if written_out % 4 != 0:
                    buffer += b"\x00" * (4 - written_out % 4)
                header = f.read(110)
                buffer += header
                if header == b"":
                    break
                assert header[:6] == b"070701", (header[:6], f.tell())
                fsize = int(header[54:62], 16)
                fnamesz = int(header[94:102], 16)
                filename = f.read(fnamesz)
                buffer += filename
                if f.tell() % 4 != 0:
                    buffer += f.read(4 - f.tell() % 4)
                # assumption: no large files are part of the archive
                filecontent = f.read(fsize)
                buffer += filecontent
                if filename == b"TRAILER!!!\0":
                    remainder = f.tell() % 512
                    if remainder != 0:
                        assert f.read(512 - remainder) == (512 - remainder) * b"\x00"
                    # recompute the padding as other cpio archives might've been in front
                    if written_out % 512 != 0:
                        buffer += b"\x00" * (512 - written_out % 512)
                if i == 0 and filename.startswith(b"lib/modules/"):
                    # skip everything in lib/modules/ in the first initrd
                    pass
                elif filename == b"TRAILER!!!\0" and i != len(sys.argv)-2:
                    # skip the trailer for all but the last
                    pass
                else:
                    sys.stdout.buffer.write(buffer)
                    written_out += len(buffer)


if __name__ == "__main__":
    main()
END
chmod +x cpiofilter.py

# chdir to not include a leading slash in the cpio archive members
env --chdir=/ sh -c 'exec find lib/modules/*'"$abiname"'/ -print0' \
  | LC_ALL=C sort -z \
  | env --chdir=/ cpio --null --quiet -o -H newc \
    >cpio.modules
{
  echo preseed.cfg
  echo "reform_${DIST%-backports}.sources"
  [ "$DIST" = "bookworm-backports" ] && echo "reform_${DIST%-backports}-backports.sources"
  echo usr/lib/post-base-installer.d/99reform
  echo usr/lib/finish-install.d/00reform
  echo var/lib/dpkg/info/reform.templates
  echo flash.bin
  [ "$SYSIMAGE" = "reform-system-ls1028a" ] && echo ls1028a-mhdpfw.bin
} | cpio --quiet -o -H newc >cpio.preseed
gzip --decompress --force initrd.gz
./cpiofilter.py initrd cpio.modules cpio.preseed | gzip >initrd-full.gz

# Create a ~146 MB image
fallocate --posix --length="$((150*1024*1000))" partition.img
/sbin/mkfs.fat --offset $((32*1024)) -F 32 --invariant partition.img 133616
mmd   -i partition.img@@16M ::extlinux
mcopy -i partition.img@@16M -v extlinux.conf ::extlinux
mmd   -i partition.img@@16M ::dtbs
mmd   -i partition.img@@16M ::dtbs/freescale
mcopy -i partition.img@@16M -v /usr/lib/linux-image-*"$abiname/$DTBPATH" ::dtbs/freescale
mcopy -i partition.img@@16M -v /boot/vmlinuz-*"$abiname" ::vmlinuz
mcopy -i partition.img@@16M -v initrd-full.gz ::initrd.gz

# The partition table layout is the same as in the images from
# http://ftp.debian.org/debian/dists/bookworm/main/installer-arm64/current/images/netboot/SD-card-images/
# The first partition starts at 16 MB which is more than enough for any
# offset at which u-boot needs to be flashed depending on the board
cat <<END | /sbin/sfdisk partition.img
label: dos
label-id: 0x0a863812
unit: sectors
sector-size: 512

start=32768, size=267232, type=c, bootable
END

# offsets given in multiples of 512 bytes
dd if=./flash.bin of="partition.img" conv=notrunc bs=512 seek="$((UBOOT_OFFSET / 512))" skip="$((FLASHBIN_OFFSET / 512))"

bmaptool create --output="partition.img.bmap" "partition.img"

: <<'-----END PGP PUBLIC KEY BLOCK-----'
-----BEGIN PGP PUBLIC KEY BLOCK-----
.
mQINBFHVWP0BEAC4vnKRkDgoQ4JrRHhDrKipbs4I0xwRSDHlhnD1bsa12PNaJytH
HUufulM5woChwGPFOH0Ex0eOzFWzQ1cHmijIIdm5h9tGSxQK+AF5lh2q9/ae1SXW
bh9u+6u8PWS1P9nxXMCN9c4ahwUb5YYCH2ThkmlhzvAeX0/hk85zecglsypUfQgO
9tp72S8CX/Lx0HX0at7xEioKgA39/ZWD4b7FktI3MX+UYMgOXsgsWqmY2gMGUp3E
3Aa6se6/63nhY3HLCCHUYS3pxP7Cnw5fI3/KJ9yBSGQ8LoNwijJtJD0XWTaUikKy
+MrifZDpfFIxvo/JJJLOXTi7nEnXZitKV5pz49/6CkhbSdAt423honj+Gn58viUw
pyMjpfCaZu4/RN8GLDMvlz2etst0HHnINIwQWPrXLubF3jqe8uhseKATO7FkgaOw
4o6xC+NZuycT7pXsb6m51y/TZfAq/eTP0TE8jMSVf1dpMoyLOcI4VciL26G7uujQ
qjdBVIgcPnv7XG9y+HqHX49pvTRo0Sum21LpbZRDCeRtYe8flbBvHzM+B+S93xSn
uppct4BFrKJ2RU7QCIpDOBvfP24cy9Nu+AphScXw5FtQOzKfz3+kosPz3uu0XCUt
xX7h8s00dT4hQQX/bMwjqa2WJNnoaqg5oIPMRNkZHPuPNcsdobSy3KJuWwARAQAB
tDNKb2hhbm5lcyBTY2hhdWVyIE1hcmluIFJvZHJpZ3VlcyA8am9zY2hAZGViaWFu
Lm9yZz6JAk4EEwEKADgWIQT4M1a74RK3RipBVS99XYxgz00+tAUCYGYPLgIbAwUL
CQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRB9XYxgz00+tHdiD/0QnED9RdlYqN8f
D/fNvHfYJboETERHlmcbPXFm0gD+JPH+g5RB0Q3zp1lT0yuSNW73bb3/Clz91jZJ
/vvslBlSuR53sNKXLOy4ZfvwP3c1wgAV9M/Saqae5RMxzv3xdZDA7NoPuTJUlrig
N7SqhsdlQcvxuhJzfFsgkwUQltCOnIgt3U0whfXoFTJISxGZWgk4zHQ4joI3tR4N
03md4jdf4KTTIhBn9uVSVWrUo/jfUS/dPNMYSlPN1JZyK9hxF2O+5qUw2eIavn7f
ofMBkTOJazIcBqz3iVHWGAVpmaLvO08c4sxBWDwx/0XV3/Kfz0R2yI3nRjsf26Ga
/BS/n71LpSTIdHBIt5pmFZokiTWWS70hR/0C9mOEtyfFQ1cUhl1cdratNVhZNqSz
T/kROaoWOujqJb3qyVscg61wiXD3i7aTTmH6s5V4xnO2fA3YGOZkO1bXbajv7apu
uB5z+Wyx6me7Xq+puNf1KMShLrqyJbIBFKZDAqM/nDp6sBe0PdeJwjn3yIreQ/Ei
DsFZyvKbPrQBVC0U2kzTDTzh8ifNKtua/twjRrATgSvTe89avCDBQw6D0fyWIcGG
/49aNI+BDHmsJp1ozjFR07se9T2qqAz8bWqysIXKVRyVjMu2bR+6qSB8E+VuN78F
uJGlaG650g3iep7ZidaoauwEJxo+FLkCDQRkqab3ARAAs9z66fY/dAmWdcfAGFBo
ukT1eXBlTBgYrIc0edMYtKxKvMWMPv29fouN5vyp7wlf3Ljh0SyqebnPumsKmfZX
OWMnZJ/oX1KnMRE4aJLtMBtiM9gcHz/8bbeiPLbBC7Sbo/fCylWrbyrQonvspRe6
8/kP3JmuWA3ZJm6PA8Gxo1FJtSg0k/0CAvjiZvea9mJdbmbe+q+pdYMlno+7sNCg
7zpSim8/6Ay79p6j1O1Xw1wyNMVtzuWEZIB9i59nno4xY7nIC8tKXunBzTIf9Xyj
gcvYV1lV+A3HCYvJxHeBYOwRfJr2KmPCJ4uND2OicFvRZ2Zgkn/gifPSTYubBgqQ
rWhNmDmSIhkcxvIDORCzrDNuK5lbtNw+4jRuHnl14NfoZEv8FeYVQzGEDPN1G+pM
V4f/QWRG5izIqScAUKfYEWu0NX1jEu6Beqjscnhv62GjKkOhuNMWjDx6J0bi9769
C0QYoLPjzTqj3FJlKUd3G6BOQovxLc5RCOVvK48jUtYrDcRthRTqlaGAO/8wH4vb
6kyLXDpdJ53CF0vqox1UY0SCaAWj+3tX7CMswIx9m4Yh+jhjj/vK52yzQp4ilsST
6bz6ft06bwzFuQ0NWVz7zqn9Rs0y0F5FLam3vUZE7R6yeDw0IKBRB/j/t7giiCP/
cwZo4EAb3hsgW36R8BCKC4EAEQEAAYkEbAQYAQoAIBYhBPgzVrvhErdGKkFVL31d
jGDPTT60BQJkqab3AhsCAkAJEH1djGDPTT60wXQgBBkBCgAdFiEEOsbrhA+lzj/z
G62A75MiH4pE/rIFAmSppvcACgkQ75MiH4pE/rKffA/+ItqHJjQnYa+3dhrXKK5D
LoGaa9K7UmUqo6w5Zyb0lYswY5UsUKz3av2D2ENHX0VZSvYBBzA1WTzN3IYuR2EF
cca4BOLH9GTLuOyc0vwwWCINBSOvJ0AvIOrLK69ieS2chIebi/AvNfblh2+VkDax
OrcCtlcupATP5KoJKFWgcwEvAx+Nuu3prjweMKadYn0tpIEOqk0BUHQ8EjdLVT4j
f0O+AEr+KpzX1N/0Scp8RIfeLj0KIXEZw0gGLyoKhTbMIVuAI3c7gsTqNECZymFs
04fXuMdwqEs4a2VCgAKUKxrLc3ytNC9tWHiAafrU4Fb3tKQmU4nKhGj411D+S4iZ
jAfYFuhka+8qGiDss69mJl43ZtNVveDETfMYQ5XZgYGvzPHZW6bHMuvSGsPPizz7
/73Gh6O7HkzPwthx5bub8NY2AwuqbAwI78YwfqYwWiJihoLrqADv8XzGjR0POHVe
q+miqmU7BXvPCQ6K1VuU15Pln1RRZRfUROFlXzkTBm5Fh7eS+12QpYI7CIGCJSUC
f2cATRoUhBFKDd0QCnQR+rNQVWJSXpLhPxwRDU+er1nN4RdMUW4vBC/Wa7Ifo7E1
wxShYCTA8MtLdG/RWYAL6ygtAsSM0tkTUbBvcm1om2Rj6BK8quLhoqJpEMV0Hm1d
3uluvjZJ+jF/yA0kI176YwYEKBAAitBHc0B8wrKcBUOOFZjWob3HtVossn6rF+cx
BB6Vo0quUOoLbbnEvNtpcNEVw8LTbzibtDdxxTEt8zYvGuM3gxSzx3KMS04SF+Xz
AjoVphSu9vZ58BURcZBZucXdayIxt5G04RArwfb8shPf4VHcIZ22dlGy/EW5XHky
41QQX8OZPIBrRUWwMoGAw/o0KzJziY0PyKFMpH0j4DSXsYuRWwpQN0byFZ+gtdCA
j5HWLmjXdOHj8/2znxzr9Rz976ecmt5NnRPkp98jBcSryf6UNxtAvWyjALpPy/dP
9hwZXbw5kYzX4kncacCgleQGpeT7SNAkPhuKdR24qT3Xng4DCjorDBldBAGtj81Z
Pll9ZlTsomRQ1xI3UIbaUIhdYY1pL1LXwhCzlh9M+kQMQsmhJOl+FlpBSEugLdIu
xG6P4U0b92Vdtt+zg5YfeAgoTPKOG8gv0sPM3uuPAmaOMds0MS16jgxC7a6fp5VK
QLZnQAAhw+0pjs93SzOaq8vJH/nUwern9DiLs8i7iwb3qo9ijOQrNx0tNtZ4IkMs
4ewNMFZO8RoV5Scy5ujI3WZQCcaAz2v3ZV9I9OOCPAiTdIeul1RwCI6dAcUEpOcH
EOE44wkf6MyNBIwcw2C8auCqzqAIabfP8gAYbQt/huqXTaOENbEbEP/BFapwSQgS
usXl1jg=
=QfCt
-----END PGP PUBLIC KEY BLOCK-----
